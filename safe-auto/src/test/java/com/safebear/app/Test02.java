package com.safebear.app;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created by Admin on 23/05/2017.
 */
public class Test02 extends BaseTest {
    @Test
    public void TestLogin(){
        //Step 1 Confirm we're on the Welcome Page
        assertTrue(welcomePage.checkCorrectPage());
        //Step 2 click on the Login link and the Login Page loads
        assertTrue(welcomePage.clickOnLogin(this.loginPage));
        //Step 3 Login with valid credentials
        assertTrue(loginPage.login(this.userPage,"test","test"));
        //Step 4 Click on the Home link
        assertTrue(loginPage.clickOnHome(this.welcomePage));
        //Step 5
        assertTrue(welcomePage.checkCorrectPage());
    }
}
