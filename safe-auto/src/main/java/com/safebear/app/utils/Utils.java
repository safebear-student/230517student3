package com.safebear.app.utils;

import org.openqa.selenium.WebDriver;

/**
 * Created by Admin on 23/05/2017.
 */
public class Utils {

    String url;

    public Boolean navigateToWebsite(WebDriver driver) {
        url = "http://automate.safebear.co.uk";
        driver.get(url);
        return driver.getTitle().startsWith("Welcome");
    }
}
