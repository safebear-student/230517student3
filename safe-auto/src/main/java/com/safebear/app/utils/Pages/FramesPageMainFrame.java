package com.safebear.app.utils.Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Admin on 23/05/2017.
 */
public class FramesPageMainFrame {

    WebDriver driver;

    public FramesPageMainFrame(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public Boolean checkCorrectPage() {
        return driver.getTitle().startsWith("Frame Page");
    }
}
